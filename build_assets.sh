#!/bin/bash

mode=$1
assetsMode=""
reactMode=""

if [ $1 = "prod" ]
then
	assetsMode="build"
	reactMode="build"
elif [ $1="watch" ]
then
        assetsMode="watch"
        reactMode="start"
elif [ $1="dev" ]
then
        assetsMode="dev"
        reactMode="dev"
fi
	
intro="Building assets in "
intro+=$mode
intro+=" mode"

echo $intro
echo "==========================="
echo "==========================="
echo "Building basics assets....."
npm run $assetsMode

echo "==========================="
echo "==========================="
echo "Building react assets......"

cd "assets/react"
npm run $reactMode

echo "==========================="
echo "==========================="
echo "Build finised !"

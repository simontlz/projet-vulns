import React from "react";
import ReactDOM from "react-dom";
import Pie from "./components/Charts/Pie";
import Scatter from "./components/Charts/Scatter";
import Speedometer from "./components/Speedometer";
import Bar from "./components/Charts/Bar";

const pie = document.getElementById('reactPieChart');
const doughnut = document.getElementById('reactDoughnutChart');
const scatter = document.getElementById('reactScatterChart');
const speedometer = document.getElementById('reactSpeedometer1');
const speedometer2 = document.getElementById('reactSpeedometer2');
const speedometer3 = document.getElementById('reactSpeedometer3');
const speedometer4 = document.getElementById('reactSpeedometer4');
const speedometer5 = document.getElementById('reactSpeedometer5');
const bar = document.getElementById('reactBar');

let props = {};

if (pie !== null) {
    if (pie.dataset.subject) {
        props.subject = pie.dataset.subject;
    }
    if (pie.dataset.label) {
        props.label = pie.dataset.label;
    }
    ReactDOM.render(<Pie {...props} />, pie);
}

if (doughnut !== null) {
    if (doughnut.dataset.subject) {
        props.subject = doughnut.dataset.subject;
    }
    if (doughnut.dataset.label) {
        props.label = doughnut.dataset.label;
    }
    ReactDOM.render(<Pie {...props} />, doughnut);
}

if (scatter !== null) {
    if (scatter.dataset.subject) {
        props.subject = scatter.dataset.subject;
    }
    if (scatter.dataset.label) {
        props.label = scatter.dataset.label;
    }
    ReactDOM.render(<Scatter {...props} />, scatter);
}

if (speedometer !== null) {
    if (speedometer.dataset.value) {
        props.value = speedometer.dataset.value;
    }
    if (speedometer.dataset.label) {
        props.label = speedometer.dataset.label;
    }
    ReactDOM.render(<Speedometer {...props} />, speedometer);
}
if (speedometer2 !== null) {
    if (speedometer2.dataset.value) {
        props.value = speedometer2.dataset.value;
    }
    if (speedometer2.dataset.label) {
        props.label = speedometer2.dataset.label;
    }
    ReactDOM.render(<Speedometer {...props} />, speedometer2);
}
if (speedometer3 !== null) {
    if (speedometer3.dataset.value) {
        props.value = speedometer3.dataset.value;
    }
    if (speedometer3.dataset.label) {
        props.label = speedometer3.dataset.label;
    }
    ReactDOM.render(<Speedometer {...props} />, speedometer3);
}
if (speedometer4 !== null) {
    if (speedometer4.dataset.value) {
        props.value = speedometer4.dataset.value;
    }
    if (speedometer4.dataset.label) {
        props.label = speedometer4.dataset.label;
    }
    ReactDOM.render(<Speedometer {...props} />, speedometer4);
}
if (speedometer5 !== null) {
    if (speedometer5.dataset.value) {
        props.value = speedometer5.dataset.value;
    }
    if (speedometer5.dataset.label) {
        props.label = speedometer5.dataset.label;
    }
    ReactDOM.render(<Speedometer {...props} />, speedometer5);
}
if (bar !== null) {
    if (bar.dataset.label) {
        props.label = bar.dataset.label;
    }
    ReactDOM.render(<Bar {...props} />, bar);
}

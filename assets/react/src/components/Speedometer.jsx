import React, {Component} from 'react';
import ReactSpeedometer from "react-d3-speedometer";

export default class Speedometer extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const {value, label} = this.props;

        return (
            <div style={{backgroundColor: '#fff', maxHeight: 200, borderRadius: '.5rem'}}>
                <h4 style={{fontFamily: 'Helvetica, Arial, sans-serif', color: 'rgb(55, 61, 63)', fontSize: 20, textAlign: 'left', paddingTop: 5, paddingLeft: 5}}>{label}</h4>
                <ReactSpeedometer
                    value={parseFloat(value)}
                    segments={5}
                    minValue={0}
                    maxValue={5}
                    maxSegmentLabels={5}
                    ringWidth={40}
                    needleColor="steelblue"
                    width={200}
                />
            </div>
            )
    }
}
import React, { Component } from 'react';
import * as urls from '../../urls';
import { Chart } from "react-google-charts";

export default class Pie extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    componentDidMount() {
        this.initializeChart();
    }

    initializeChart() {
        const { subject, label } = this.props
        let url = '';
        let pieType = 'PieChart';
        let isDoughnut = false;
        if (subject) {
            if (subject === 'vuln-level') {
                url = urls.VULN_LEVEL
            } else if (subject === 'vuln-duration') {
                url = urls.VULN_DURATION;
                isDoughnut = true;
            }
        }
        fetch(url)
            .then((response) => response.json())
            .then((response) => {
                let options = {
                    title: label,
                    tooltip: {
                        showColorCode: true
                    },
                    legend: {
                        position: "bottom",
                        alignment: "center",
                        textStyle: {
                            color: "blue",
                            fontSize: 14
                        }
                    },
                    chartArea: {
                        left: 0,
                        top: 0,
                        width: '100%',
                        height: '80%',
                    }
                };
                if (isDoughnut) {
                    options.pieHole = 0.4;
                }
                options.slices = [];
                for (let color of response.colors) {
                    options.slices.push({color: color});
                }
                this.setState({
                    data: response.data,
                    loading: false,
                    colors: response.colors,
                    options: options,
                    pieType: pieType,
                    isDoughnut: isDoughnut
                })
            })
            .catch((error) => {console.log(error)})
    }

    elementClick(el) {
        const label = this.state.indexes[el[0]['_index']];
        alert(label + ' clicked !')
    }

    render() {
        return (
            <div>
                {!this.state.loading ?
                    <Chart chartType={this.state.pieType}
                           loader={<img src={'/img/loader.gif'} alt={'loader'} className={'loader'}/>}
                           width={'100%'}
                           height={'200px'}
                           data={this.state.data}
                           options={this.state.options}
                           rootProps={{ 'data-testid': '1' }}
                    />
                    : null}
            </div>
        );

    }
}
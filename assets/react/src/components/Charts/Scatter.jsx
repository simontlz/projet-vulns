import React, {Component} from 'react';
import * as urls from '../../urls';
import Chart from "react-apexcharts";


export default class Scatter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
        }
    }

    componentDidMount() {
        this.initializeChart();
    }

    initializeChart() {
        const {label} = this.props
        fetch(urls.VULN_SUMMARY)
            .then((response) => response.json())
            .then((response) => {
                let options = {
                    dataLabels: {
                        enabled: false
                    },
                    toolbar: {
                        show: false,
                        tools: {
                            download: false,
                            selection: true,
                            zoom: true,
                            zoomin: false,
                            zoomout: false,
                            pan: true,
                            reset: true,
                            customIcons: []
                        },
                        autoSelected: 'zoom',
                    },
                    legend: {
                        show: true,
                        showForSingleSeries: false,
                        floating: false,
                        position: 'bottom',
                        verticalAlign: 'middle',
                        align: 'center',
                        labels: {
                            useSeriesColors: true
                        }
                    },
                    title: {
                        text: label,
                        style: {
                            fontSize: '20px',
                        }
                    },
                    labels: response.labels,
                    grid: {
                        xaxis: {
                            showLines: true
                        },
                        yaxis: {
                            showLines: true
                        },
                    },
                    xaxis: {
                        type: 'datetime',

                    },
                    yaxis: {
                        max: 70
                    },
                };
                for (let serie in response.series) {
                    for (let day in response.series[serie].data) {
                        response.series[serie].data[day][0] = new Date(response.series[serie].data[day][0].date).getTime()
                    }
                }
                this.setState({
                    data: response.series,
                    loading: false,
                    options: options,
                })
            })
            .catch((error) => {
                console.log(error)
            })
    }

    render() {
        return (
            <div>
                {!this.state.loading ?
                    <Chart type={'scatter'}
                           series={this.state.data}
                           options={this.state.options}
                           zoom={{type: 'xy'}}
                           height={330}
                    />
                    :
                    <img src={'/img/loader.gif'} alt={'loader'} className={'loader'}/>
                }
            </div>
        );

    }
}
import React, { Component } from 'react';
import * as urls from '../../urls';
import Chart from "react-apexcharts";


export default class Bar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    componentDidMount() {
        this.initializeChart();
    }

    initializeChart() {
        const { label } = this.props

        const colors = ['#fd6585', '#69a9d1', '#fecd60', '#C569D1', '#fd6585', '#69a9d1', '#fecd60', '#C569D1'];
        const data = [
            {
                data: [2, 3, 5, 1, 5, 4, 5, 4]
            }
        ];
        let options = {
            colors: colors,
            xaxis: {
                categories: ['Sept 2018', 'Oct 2018', 'Nov 2018', 'Déc 2018', 'Jan 2019', 'Fév 2019', 'Mar 2019', 'Avr 2019'],
                labels: {
                    style: {
                        colors: colors,
                        fontSize: '14px'
                    }
                }
            },
            title: {
                text: label,
                style: {
                    fontSize: '20px',
                }
            },

        };
        this.setState({
            loading: false,
            data: data,
            colors: colors,
            options: options,
        })
    }

    render() {
        return (
            <div>
                {!this.state.loading ?
                    <Chart type={'bar'}
                           options={this.state.options}
                           series={this.state.data}
                           colors={this.state.colors}
                    />
                    :
                    <img src={'/img/loader.gif'} alt={'loader'} className={'loader'}/>
                }
            </div>
        );

    }
}
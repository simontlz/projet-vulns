import React, { Component } from 'react';
import * as urls from '../../urls';
import Chart from "react-apexcharts";


export default class Pie extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    componentDidMount() {
        this.initializeChart();
    }

    componentDidUpdate() {
        if (this.divElement) {
            console.log(this.divElement);
        }
    }

    initializeChart() {
        const { subject, label } = this.props
        let url = '';
        let isDoughnut = false;
        if (subject) {
            if (subject === 'vuln-level') {
                url = urls.VULN_LEVEL
            } else if (subject === 'vuln-duration') {
                url = urls.VULN_DURATION;
                isDoughnut = true;
            }
        }
        fetch(url)
            .then((response) => response.json())
            .then((response) => {
                let indexes = [];
                for (let index of response.labels) {
                    indexes.push(index);
                }
                let options = {
                    dataLabels: {
                        enabled: true,
                        formatter: (val) => {
                            if (isDoughnut) {
                                return response.series[0] + ' jours';
                            } else {
                                return Math.round(val) + "%"
                            }
                        }
                    },
                    legend: {
                        show: true,
                        showForSingleSeries: false,
                        floating: false,
                        position: 'bottom',
                        verticalAlign: 'middle',
                        align: 'center',
                        labels: {
                            useSeriesColors: true
                        }
                    },
                    title: {
                        text: label,
                        style: {
                            fontSize: '20px',
                        }
                    },
                    chart: {
                        events: {
                            dataPointSelection: (event, chartContext, config) => this.elementClick(event, chartContext, config),
                        }
                    },
                    colors: response.colors,
                    labels: response.labels
                };
                this.setState({
                    data: response.series,
                    loading: false,
                    options: options,
                    isDoughnut: isDoughnut,
                    indexes: indexes
                })
            })
            .catch((error) => {console.log(error)})
    }

    elementClick(event, chartContext, config) {
        const label = this.state.indexes[config.dataPointIndex];
        alert(label + ' clicked !')
    }

    render() {
        return (
            <div>
                {!this.state.loading ?
                    <Chart type={this.state.isDoughnut ? 'donut' : 'pie'}
                           series={this.state.data}
                           options={this.state.options}
                           ref={ (divElement) => this.divElement = divElement}
                    />
                    :
                    <img src={'/img/loader.gif'} alt={'loader'} className={'loader'}/>
                }
            </div>
        );

    }
}
export const BLUE = '#69a9d1';
export const VIOLET = '#C569D1';
export const GREEN = '#75D169';
export const ORANGE = '#D19169';
export const PINK = '#fd6585';
export const RED = '#bf2932';
export const YELLOW = '#fecd60';
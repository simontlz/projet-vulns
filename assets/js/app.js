/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');

$(document).ready(function () {
    if (localStorage.getItem('navbarCollapsed') && localStorage.getItem('navbarCollapsed') === 'false') {
        $('#sidebar-collapsed').fadeOut('fast', () => {
            $('#page-left-part').removeClass('col-sm-1').addClass('col-sm-2');
            $('#page-right-part').removeClass('col-sm-11').addClass('col-sm-10');
            $('#sidebar-expended').fadeIn();
            $('#page-left-part').removeClass('collapsed').addClass('expended')
            localStorage.setItem('navbarCollapsed', false);
        });
    }
    $('#menu-icon').on('click', () => {
        if ($('#page-left-part').hasClass('expended')) {
            $('#sidebar-expended').fadeOut('fast', () => {
                $('#page-left-part').removeClass('col-sm-2').addClass('col-sm-1');
                $('#page-right-part').removeClass('col-sm-10').addClass('col-sm-11');
                $('#sidebar-collapsed').fadeIn();
                $('#page-left-part').removeClass('expended').addClass('collapsed');
                localStorage.setItem('navbarCollapsed', true);
            });
        } else if ($('#page-left-part').hasClass('collapsed')) {
            $('#sidebar-collapsed').fadeOut('fast', () => {
                $('#page-left-part').removeClass('col-sm-1').addClass('col-sm-2');
                $('#page-right-part').removeClass('col-sm-11').addClass('col-sm-10');
                $('#sidebar-expended').fadeIn();
                $('#page-left-part').removeClass('collapsed').addClass('expended')
                localStorage.setItem('navbarCollapsed', false);
            });
        }
    });

    if ($(location).attr("pathname") === '/') {
        $('#logs-link').on('click', () => {
            $('#logs-link a').addClass('active');
            $('#scans-link a').removeClass('active');
            $('#dashboard-scans-view').fadeOut('fast', () => {
                $('#dashboard-logs-view').fadeIn('fast');
            })
        })
        $('#scans-link').on('click', () => {
            $('#scans-link a').addClass('active');
            $('#logs-link a').removeClass('active');
            $('#dashboard-logs-view').fadeOut('fast', () => {
                $('#dashboard-scans-view').fadeIn('fast');
            })
        })
    }

    if ($('#new-scan')) {
        $('#ip').on('click', () => {
            $('#scanner').fadeOut('fast', () => {
                $('#ipRangeContent').fadeOut('fast', () => {
                    $('#ipContent').fadeIn('fast', () => {
                        $('.step-1-btn').fadeIn();
                    });
                });
            });
        })
        $('#ipRange').on('click', () => {
            $('#scanner').fadeOut('fast', () => {
                $('#ipContent').fadeOut('fast', () => {
                    $('#ipRangeContent').fadeIn('fast', () => {
                        $('.step-1-btn').fadeIn();
                    });
                });
            });
        })
        $('#ipContent .validate-ip').on('click', () => {
            if ($('#startIp').val() === '') {
                $('#ipError').fadeIn()
            } else {
                $('.step-1-btn').fadeOut('fast', () => {
                    $('#ipError').fadeOut('fast', () => {
                        $('#scanner').fadeIn();
                    })
                });
            }
        })
        $('#ipRangeContent .validate-ip').on('click', () => {
            if ($('#endIp').val() === '') {
                $('#ipRangeError').fadeIn()
            } else {
                $('.step-1-btn').fadeOut('fast', () => {
                    $('#ipRangeError').fadeOut('fast', () => {
                        $('#scanner').fadeIn();
                    })
                });
            }
        })
    }
});

<?php

namespace App\Controller;

use App\Entity\Log;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DashboardController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $logs = $this->getDoctrine()->getRepository(Log::class)->findAll();

        return $this->render('dashboard/index.html.twig', ['logs' => $logs]);
    }
}
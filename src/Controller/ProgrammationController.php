<?php

namespace App\Controller;

use App\Entity\Programmation;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ProgrammationController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Programmation::class);
        $scripts = $repo->findAll();

        return $this->render('programmation/index.html.twig', [
            'scripts' => $scripts
        ]);
    }

    public function new(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Programmation::class);
        $session = $this->get('session');
        $script = [];
        if ($request->query->has('id')) {
            $programmation = $repo->find($request->query->get('id'));
            $script = $programmation;
        }

        if ($request->getMethod() === "POST") {
            $fields = $request->request->all();

            if (sizeof($fields) < 3) {
                $session->getFlashBag()->add(
                    'error',
                    'Tous les champs doivent être rempli'
                );

                return $this->redirectToRoute('programmation_new', [
                    'script' => $script
                ]);
            } elseif (sizeof($repo->findBy(['type' => $fields['type']])) >= 1 && !$request->query->has('id')) {
                $session->getFlashBag()->add(
                    'error',
                    'Un seul script est autorisé par scanner'
                );
                return $this->redirectToRoute('programmation_new', [
                    'script' => $script
                ]);
            }
            try {
                if (!$request->query->has('id')) {
                    $programmation = new Programmation();
                }
                $programmation->setType($fields['type'])
                    ->setFrequency($fields['frequency']);

                $days = [];
                foreach ($fields['weekday'] as $day => $val) {
                    $days[] = $day;
                }

                $programmation->setDays($days);

                $em->persist($programmation);
                $em->flush();

                $session->getFlashBag()->add(
                    'success',
                    'Votre script a bien été ajouté/mis à jour.'
                );

                $scripts = $repo->findAll();

                return $this->redirectToRoute('programmation', [
                    'scripts' => $scripts
                ]);
            } catch (\Exception $e) {
                $session->getFlashBag()->add(
                    'error',
                    'L\erreur suivante: ' . $e->getMessage() . ' est apparue. Veuillez réessayer ultérieurement.'
                );
                return $this->redirectToRoute('programmation_new', [
                    'script' => $script
                ]);
            }
        }
        return $this->render('programmation/new.html.twig', [
            'script' => $script
        ]);
    }
}
<?php

namespace App\Controller;

use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class AjaxController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function vulnLevel()
    {
        $data = [
            'labels' => [
                '1', '2', '3', '4'
            ],
            'series' => [
                20, 70, 10, 40
            ],
            'colors' => [
                '#fd6585', '#69a9d1', '#fecd60', '#C569D1'
            ]

        ];

        return new JsonResponse($data);
    }

    public function vulnDuration()
    {
        $data = [
            'labels' => [
                'Durée'
            ],
            'series' => [
                13
            ],
            'colors' => [
                '#75D169'
            ]

        ];

        return new JsonResponse($data);
    }

    public function vulnSummary()
    {
        $data = [
            'series' => [
                [
                    'name' => '1',
                    'data' => [
                        [new \DateTime(), 20],
                        [new \DateTime('2019-04-16'), 30],
                        [new \DateTime('2019-04-16 12:30:00'), 40],
                        [new \DateTime('2019-04-17'), 5],
                        [new \DateTime('2019-04-18'), 30],
                        [new \DateTime('2019-04-19'), 30],
                        [new \DateTime('2019-04-20'), 33],
                        [new \DateTime('2019-04-21'), 18],
                    ],
                ],
                [
                    'name' => '2',
                    'data' => [
                        [new \DateTime(), 40],
                        [new \DateTime('2019-04-16'), 20],
                        [new \DateTime('2019-04-16 12:30:00'), 30],
                        [new \DateTime('2019-04-17'), 22],
                        [new \DateTime('2019-04-18'), 1],
                        [new \DateTime('2019-04-19'), 34],
                        [new \DateTime('2019-04-20'), 29],
                        [new \DateTime('2019-04-21'), 50],
                    ],
                ],
                [
                    'name' => '3',
                    'data' => [
                        [new \DateTime(), 30],
                        [new \DateTime('2019-04-16'), 10],
                        [new \DateTime('2019-04-16 12:30:00'), 20],
                        [new \DateTime('2019-04-17'), 18],
                        [new \DateTime('2019-04-18'), 11],
                        [new \DateTime('2019-04-19'), 28],
                        [new \DateTime('2019-04-20'), 47],
                        [new \DateTime('2019-04-21'), 2],
                    ],
                ],
                [
                    'name' => '4',
                    'data' => [
                        [new \DateTime(), 50],
                        [new \DateTime('2019-04-16'), 26],
                        [new \DateTime('2019-04-16 12:30:00'), 36],
                        [new \DateTime('2019-04-17'), 35],
                        [new \DateTime('2019-04-18'), 14],
                        [new \DateTime('2019-04-19'), 5],
                        [new \DateTime('2019-04-20'), 2],
                        [new \DateTime('2019-04-21'), 9],
                    ],
                ],
            ]
        ];

        return new JsonResponse($data);
    }
}
<?php

// src/Command/CreateUserCommand.php
namespace App\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class AssetsCommand extends ContainerAwareCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'build-assets';

    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Used to build react and basic assets in prod or dev mode or to watch it.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command will build automatically the react\'s and basic\'s assets in the specified mode. If nothing is specified, it will be built in production mode.')
        ;

        $this
            // ...
            ->addOption('mode', '-m', InputArgument::OPTIONAL, 'Build mode')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mode = $input->getOption('mode');
        $reactMode = 'build';
        $assetsMode = 'build';

        if ($mode === 'dev') {
            $assetsMode = 'dev';
            $reactMode = 'dev';
        } elseif ($mode === 'watch') {
            $assetsMode = 'watch';
            $reactMode = 'start';
        }

        // outputs multiple lines to the console (adding "\n" at the end of each line)
        $output->writeln([
            'Building assets in ' . (is_null($input->getOption('mode')) ? 'prod'  : $input->getOption('mode')) . ' mode',
                '============',
                ''
            ]
        );

        $output->writeln([
                'Building basic assets ...'
            ]
        );

        $runBuild = new Process(['/usr/local/bin/npm run ' . $assetsMode], $this->getContainer()->getParameter('kernel.project_dir'));
        $runBuild->enableOutput();
        $runBuild->run();

        $output->writeln($runBuild->getOutput());
        if (!$runBuild->isSuccessful()) {
            throw new ProcessFailedException($runBuild);
        }

        $output->writeln([
                'Building react assets ...'
            ]
        );

        $runBuild = new Process(['/usr/local/bin/npm run ' . $reactMode], $this->getContainer()->getParameter('kernel.project_dir') . '/assets/react');
        $runBuild->enableOutput();
        $runBuild->run();

        $output->writeln($runBuild->getOutput());
        if (!$runBuild->isSuccessful()) {
            throw new ProcessFailedException($runBuild);
        }


        /*$process = new Process(['/usr/local/bin/npm run --prefix assets/react ' . $reactMode]);
        $process->run();

        $output->writeln($process->getOutput());

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $output->writeln([
                'Building basic assets ...'
            ]
        );

        $process = new Process(['/usr/local/bin/npm run ' . $assetsMode]);
        $process->run();

        $output->writeln($process->getOutput());
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $output->writeln([
                'Assets built !'
            ]
        );*/
    }
}

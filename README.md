# Security project

## Wazuh :
### What is Wazuh ?
>Wazuh is a free, open-source host-based intrusion detection system (HIDS). It performs log analysis, integrity checking, Windows registry monitoring, rootkit detection, time-based alerting, and active response. It provides intrusion detection for most operating systems, including Linux, OpenBSD, FreeBSD, OS X, Solaris and Windows. Wazuh has a centralized, cross-platform architecture allowing multiple systems to be monitored and managed.

### How does it work with the project ?
Once you will have your wazuh installed (follow [this link](https://documentation.wazuh.com/current/index.html) 
to install one) as well as the Symfony project, you will first have to configure an 
API key ([how to configure wazuh api](https://documentation.wazuh.com/current/user-manual/api/index.html)) 
and then you will already be able to use it. How? Wazuh provides a REST API that you can call from a Symfony Controller.
<br/>

If you already have an agent, and it should be the case if you followed the Wazuh installation doc, 
installed in the machine which provides scans results and logs you want Wazuh to
parse and analyse, you can call the Wazuh's REST API to receive the results.
Usually, the basic url to call should be something like https://localhost:55000. Then, adding parameters to this URI (for exemple /cluster/:node_id/logs), will give you the results of the logs parsing, results of scan analysis, etc.
<br/>

You will find all API URIs following [this link](https://documentation.wazuh.com/current/user-manual/api/reference.html).
<br/>

That's it ! Up to you then to reformat the JSON returned from the API directly in your Controller so that the React part won't have
any more work to do and will be able to display it correctly.

## Symfony installation
* Run the following commands to download and setup Symfony 4
```
git clone https://gitlab.com/simontlz/projet-vulns.git SecuProject
cd SecuProject
composer install
```

## Elastic search installation :
This section is usefull if you haven't any elastic search installed (remembering that Wazuh comes with an inboxed one)
* Run the following commands to download and setup elastic search
```
docker pull elasticsearch:6.6.1
docker network create EC-network
docker run -d --name elasticsearch-test --net EC-network -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:6.6.1
cd {project_directory}
docker-compose up
```

## React installation :
* Run the following commands to download and setup react
```
cd assets/react
npm install
```
* While developing the app, it could be interesting to not rebuild the react part after each change. If so, run the following command :
```
npm run start
```
* If you want to build your project in a dev mode, run :
```
npm run dev
```
* When everything is clear, run the following to have all your react minified and fast :
```
npm run build
```

## Assets installation :
* Run the following commands to download and setup the assets
```
yarn install
```
* While developing the app, it could be interesting to not rebuild the assets part after each change. If so, run the following command :
```
npm run watch
```
* If you want to build your project in a dev mode, run :
```
npm run dev
```
* When everything is clear, run the following to have all your assets minified and fast :
```
npm run build
```
<br/>
<br/>

For convenience reasons I also created a shell script so all the assets can be compiled with one simple command. While being located at the project base, run :
```
./build_assets.sh [prod or dev]
``` 
The prod argument will compile & minify your assets while the dev argument will just compile your assets to make it easier to debug. 

